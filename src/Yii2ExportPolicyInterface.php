<?php declare(strict_types=1);

/*
 * This file is part of the yii2-extended/yii2-export-policy-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Extended\Yii2Export;

use Iterator;
use RuntimeException;
use Stringable;
use yii\db\ActiveQueryInterface;

/**
 * Yii2ExpoortPolicyInterface class file.
 * 
 * This interface specifies how the tables are partitionned. It returns 
 * active query to get the data for that specific partition, and a path that
 * is where to put the formatted export.
 * 
 * @author Anastaszor
 * @extends \Iterator<ActiveQueryInterface>
 */
interface Yii2ExportPolicyInterface extends Iterator, Stringable
{
	
	/**
	 * Gets an unique amongst queries suitable to make caching indexes.
	 * 
	 * @return string
	 */
	public function getCurrentQueryId() : string;
	
	/**
	 * Gets the query to get the data specific to current partition of the
	 * table data. This MUST NOT be null if self::valid() is true, and MUST 
	 * be null if self::valid() is false.
	 * 
	 * @return ActiveQueryInterface
	 */
	public function getCurrentActiveQuery() : ActiveQueryInterface;
	
	/**
	 * Gets the path where to export the data. This path MUST be valid.
	 * 
	 * This method MUST return a fully qualified path for a file, but with
	 * no extensions. The extension of this path will be added to this path
	 * by the export format.
	 * 
	 * @return string
	 * @throws RuntimeException if the path data cannot be created, or made
	 *                          valid by any means
	 */
	public function getCurrentPath() : string;
	
}
